package com.example.rash1k.workservices;

import android.app.PendingIntent;
import android.app.Service;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.util.Log;

import java.util.Arrays;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

import static android.preference.PreferenceManager.getDefaultSharedPreferences;


public class SomeService extends Service {
    public static final String TAG = "SomeService";
    public static final String ACTION_PROVIDE_SLIDE =
            "com.example.rash1k.workservices.intent.action.PROVIDE_SLIDE";
    public static final String ACTION_SLIDE_SHOW =
            "com.example.rash1k.workservices.intent.action.SLIDE_SHOW";
    public static final int PROVIDE_RESULT_CODE = 100;
    public static final int SLIDE_SHOW_RESULT_CODE = 200;

    private static boolean sIsStartSlideShow = false;
    private static boolean sDirection = true;
    private static int[] sImageIdentifier = Utils.getDrawableIdentifier();
    //    private Timer mTimer;
//    private TimerTask mTimerTask;
//    private static int sSlideIndex;
    private static int mCommandCount;
    private ExecutorService mExecutorService;
    private CountDownTimer mCountDownTimer;

    @Override
    public void onCreate() {
        super.onCreate();
//        Log.d(TAG, "SomeService onCreate: ");
        mExecutorService = Executors.newFixedThreadPool(2);
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Log.d(TAG, "onStartCommand: ");
        if (intent != null) {
            String action = intent.getAction();
            if (action.equals(ACTION_PROVIDE_SLIDE)) {
                incrementAndDecrementCommandCount(true, startId);
//                Log.d(TAG, "mCommandCount: " + mCommandCount);
                startBackgroundTask(intent, startId);

            } else if (action.equals(ACTION_SLIDE_SHOW)) {
                sIsStartSlideShow = !sIsStartSlideShow;
//                Log.d(TAG, "sIsStartSlideShow: " + sIsStartSlideShow);

                if (sIsStartSlideShow) {
                    incrementAndDecrementCommandCount(true, startId);
//                    Log.d(TAG, "mCommandCount: " + mCommandCount);
                    runSlideShow(intent, startId);
                }
                if (!sIsStartSlideShow && mCountDownTimer != null) {
                    mCountDownTimer.cancel();
                    mExecutorService.shutdownNow();
                    incrementAndDecrementCommandCount(false, startId);
                }
            }
        }

        return START_REDELIVER_INTENT;
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        Log.d(TAG, "onBind: ");
        return null;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        Log.d(TAG, "SomeService onDestroy: ");
        mExecutorService.shutdown();
    }

    private void startBackgroundTask(final Intent intent, final int startId) {
        mExecutorService.submit(new Runnable() {
            @Override
            public void run() {
                String action = intent.getAction();
                if (action != null) {
                    final Uri uri = intent.getData();
                    int drawableId = Integer.valueOf(uri.getLastPathSegment());

                    int len = sImageIdentifier.length;
                    int drawableIndex = 0;
                    for (int i = 0; i < len; i++) {
                        drawableIndex = Arrays.binarySearch(sImageIdentifier, 0, len, drawableId);
                    }

                    getDefaultSharedPreferences(SomeService.this)
                            .edit().putInt(getString(R.string.drawable_index_key), drawableIndex).apply();

                    final PendingIntent pendingIntent = intent.getParcelableExtra(
                            MainActivity.PARAM_PENDING_INTENT);

                    Bitmap bitmap = BitmapFactory.decodeResource(getResources(), drawableId);

                    intent.putExtra(MainActivity.PARAM_BITMAP, bitmap);

                    try {
                        pendingIntent.send(SomeService.this, PROVIDE_RESULT_CODE, intent);
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();
                    }
                    bitmap.recycle();
                    bitmap = null;
                    incrementAndDecrementCommandCount(false, startId);
                }
            }
        });
    }


    private void runSlideShow(final Intent intent, final int startId) {
//        mExecutorService.submit(new Runnable() {
//            @Override
//            public void run() {
        Log.d(TAG, "runSlideShow: ");

        final SharedPreferences sp = getDefaultSharedPreferences(SomeService.this);
        final int[] identifiers = sImageIdentifier;
        final PendingIntent pendingIntent = intent.getParcelableExtra(
                MainActivity.PARAM_PENDING_INTENT);

        if (mCountDownTimer == null) {
            final int millisInFuture = 5000;
            final int millisInterval = 5000;
            mCountDownTimer = new CountDownTimer(millisInFuture, millisInterval) {
                @Override
                public void onTick(long millisUntilFinished) {
                }

                @Override
                public void onFinish() {

                    int drawableIndex = sp.getInt(
                            getString(R.string.drawable_index_key), -1);

                    sDirection = sp.getBoolean(getString(R.string.direction_key), true);

                    if (sDirection) {
                        if (drawableIndex >= 0 || drawableIndex == -1) {
                            drawableIndex = ++drawableIndex % identifiers.length;
                        }
                    } else {
                        if (drawableIndex > 0) {
                            drawableIndex = --drawableIndex % identifiers.length;
                        } else if (drawableIndex <= 0) {
                            drawableIndex = identifiers.length - 1;
                        }
                    }

                    sp.edit().putInt(getString(R.string.drawable_index_key),
                            drawableIndex).apply();
                    int drawableId = identifiers[drawableIndex];
                    Log.d(TAG, "drawableIndex: " + drawableIndex);
                    intent.putExtra(Intent.EXTRA_SUBJECT, drawableId);

                    try {
                        pendingIntent.send(SomeService.this, SLIDE_SHOW_RESULT_CODE, intent);
                    } catch (PendingIntent.CanceledException e) {
                        e.printStackTrace();

                    }

                    if (sIsStartSlideShow) mCountDownTimer.start();
                    else {
                        mCountDownTimer.cancel();
                        if (--mCommandCount <= 0) stopSelf(startId);
                    }
                }
            }.start();
        }
//            }
//        });
    }

    private void incrementAndDecrementCommandCount(boolean increaseOrDecrease, int startId) {
        synchronized (this) {
            if (increaseOrDecrease) ++mCommandCount;
            else if (--mCommandCount <= 0) stopSelf(startId);
        }
    }

    private void runSlideshow() {
      /*  if (mTimer != null) {
            mTimer.cancel();
        } else {
            mTimer = new Timer("Timer", true);
            mTimerTask = new TimerTask() {
                @Override
                public void run() {

                }
            };
//            mTimer.schedule();
        }*/
    }

    private void stopService() {
        stopSelf();
    }
}
