package com.example.rash1k.workservices;

import android.app.PendingIntent;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;


public class MainActivity extends AppCompatActivity {

    public static final String TAG = "MainActivity";
    public static final int SERVICE_REQUEST_CODE = 1;
    public static final String PARAM_BITMAP = "bitmap";
    public static final String PARAM_PENDING_INTENT = "pending_intent";
    private static final int[] DRAWABLE_IDENTIFIER = Utils.getDrawableIdentifier();
    private static int sSaveIndex;
    private ImageView mImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mImageView = (ImageView) findViewById(R.id.imageView);

        if (savedInstanceState == null) {
            PreferenceManager.getDefaultSharedPreferences(this).edit().clear().apply();
        }
    }


    public void onClick(View view) {
        final int[] identifiers = DRAWABLE_IDENTIFIER;
        int len = identifiers.length;

        SharedPreferences sp = PreferenceManager.getDefaultSharedPreferences(this);
        String drawableIndexKey = getString(R.string.drawable_index_key);
        int drawableIndex = sp.getInt(drawableIndexKey, -1);

        if (drawableIndex <= 0) {
            drawableIndex = 0;
        }

        Intent intent = new Intent(this, SomeService.class);
        PendingIntent pendingIntent = createPendingResult(SERVICE_REQUEST_CODE, intent,
                PendingIntent.FLAG_UPDATE_CURRENT);
        intent.putExtra(PARAM_PENDING_INTENT, pendingIntent);
        boolean direction = true;
        switch (view.getId()) {
            case R.id.button_next_slide:
                direction = true;
                intent.setAction(SomeService.ACTION_PROVIDE_SLIDE);
                if (drawableIndex == len - 1) {
                    return;
                }
                if (drawableIndex >= 0) {
                    drawableIndex = ++drawableIndex % len;
                    sSaveIndex = drawableIndex;
                }
                Log.d(TAG, "drawableIndex: " + drawableIndex);
                break;
            case R.id.button_previous_slide:
                direction = false;
                Log.d(TAG, "drawableIndex: " + drawableIndex);
                if (drawableIndex == 0) return;
                intent.setAction(SomeService.ACTION_PROVIDE_SLIDE);
                drawableIndex = --drawableIndex % len;
                sSaveIndex = drawableIndex;
                Log.d(TAG, "drawableIndex: " + drawableIndex);
                break;
            case R.id.button_slideshow:
                intent.setAction(SomeService.ACTION_SLIDE_SHOW);
                startService(intent);
                return;
        }
        sp.edit()
                .putInt(drawableIndexKey, drawableIndex)
                .putBoolean(getString(R.string.direction_key), direction)
                .apply();

        intent.setData(Uri.parse("android.resource://" + getPackageName() + "/"
                + identifiers[drawableIndex]));
        startService(intent);
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        Log.d(TAG, "onSaveInstanceState: ");
        outState.putInt(getString(R.string.save_index), sSaveIndex);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        sSaveIndex = savedInstanceState.getInt(getString(R.string.save_index));
        if (sSaveIndex < 0) {
            sSaveIndex = R.drawable.a;
            mImageView.setImageResource(sSaveIndex);
        }
        mImageView.setImageResource(DRAWABLE_IDENTIFIER[sSaveIndex]);

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, final Intent data) {
        if (requestCode == SERVICE_REQUEST_CODE && resultCode == SomeService.PROVIDE_RESULT_CODE) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    final Bitmap bitmap = data.getParcelableExtra(PARAM_BITMAP);
                    mImageView.post(new Runnable() {
                        @Override
                        public void run() {
                            mImageView.setImageBitmap(bitmap);
//                            bitmap.recycle();
                        }
                    });
                }
            }).start();

        } else if (requestCode == SERVICE_REQUEST_CODE &&
                resultCode == SomeService.SLIDE_SHOW_RESULT_CODE) {
            mImageView.setImageResource(data.getIntExtra(Intent.EXTRA_SUBJECT, 0));
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
//        stopService(new Intent(this, SomeService.class));
    }
}
